import { Routes, Route } from 'react-router-dom';
import './App.css'
import Layout from './Components/Layout'
import Home from './Pages/Home/Home';
import Operator from './Pages/Operator'
import Dialog from './Pages/Dialog'
import Coordinator from './Pages/Coordinator'
import Administrator from './Pages/Administrator/Administrator';
import Mainadministrator from './Pages/Mainadministrator/Mainadministrator';
import Help from './Pages/Help/Help';
import ScriptConstructor from './Pages/ScriptConstructor/ScriptConstructor';
import ProtectedRoute from './Components/ProtectedRoute';
import Allscripts from './Pages/Allscripts/Allscripts';
import Templates from './Pages/Templates/Templates';
import Drafts from './Pages/Drafts/Drafts';
import Comments from './Pages/Comments/Comments';

/*
    ProtectedRoute - route for authorized users (based on state in redux)
 */

function App() {
    return (
        <div className='App'>
            <Layout>
                <Routes>
                    <Route path='/' element={<Home />} />
                    <Route path='/operator' element={<Operator />} />
                    <Route path='/dialog' element={<Dialog />} />{/*/operator/dialog  */}
                    <Route path='/coordinator' element={<Coordinator />} />
                    <Route path='/help' element={<Help />} />
                    <Route
                        path='/administrator'
                        element={
                            <ProtectedRoute>
                                <Administrator />
                            </ProtectedRoute>
                        }
                    />
                    <Route
                        path='/mainadministrator'
                        element={
                            <ProtectedRoute>
                                <Mainadministrator />
                            </ProtectedRoute>
                        }
                    />
                    <Route
                        path='/constructor'
                        element={
                            <ProtectedRoute>
                                <ScriptConstructor />
                            </ProtectedRoute>
                        }
                    />
                    <Route
                        path='/administrator/allscripts'
                        element={
                            <ProtectedRoute>
                                <Allscripts/>
                            </ProtectedRoute>
                        }
                    />
                    <Route
                        path='/administrator/templates'
                        element={
                            <ProtectedRoute>
                                <Templates/>
                            </ProtectedRoute>
                        }
                    />
                    <Route
                        path='/administrator/drafts'
                        element={
                            <ProtectedRoute>
                                <Drafts/>
                            </ProtectedRoute>
                        }
                    />
                    <Route
                        path='/administrator/comments'
                        element={
                            <ProtectedRoute>
                                <Comments/>
                            </ProtectedRoute>
                        }
                    />
                </Routes>
            </Layout>
        </div>
    )
}

export default App;