import css from "./Administrator.module.css";
import SideBarMenu from "../../Components/SideBarMenu/SideBarMenu";
/* import { Button } from "../../Components/Button/Button";
import LinkButton from "../../Components/LinkButton/LinkButton";
import Table from "../../Components/Table/Table";
import plusIcon from "../../Assets/images/btn-plus-icon.svg";
import Notifications from "../../Components/Notifications"; */
import { useDispatch } from "react-redux";
/* import { clearState } from "../../store/slices/nodesSlice"; */
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Plus from "../../Components/Plus/Plus";

/* const drafts = [
    { id: 1, name: 'Название скрипта', createdAt: 'Время создания' },
    { id: 2, name: 'Название скрипта', createdAt: 'Время создания' },
    { id: 3, name: 'Название скрипта', createdAt: 'Время создания' },
    { id: 4, name: 'Название скрипта', createdAt: 'Время создания' },
    { id: 5, name: 'Название скрипта', createdAt: 'Время создания' },
    { id: 6, name: 'Название скрипта', createdAt: 'Время создания' },
    { id: 7, name: 'Название скрипта', createdAt: 'Время создания' }
]

const scripts = [
    { id: 1, index: '#', name: 'Название скрипта', createdAt: 'Время создания', status: 'Статус' },
    { id: 2, index: '#', name: 'Название скрипта', createdAt: 'Время создания', status: 'Статус' },
    { id: 3, index: '#', name: 'Название скрипта', createdAt: 'Время создания', status: 'Статус' },
    { id: 4, index: '#', name: 'Название скрипта', createdAt: 'Время создания', status: 'Статус' }
] */

const Administrator = () => {
  const dispatch = useDispatch();
  const [activeModal, setActiveModal] = useState(false);
  const [comments, setComments] = useState("info");
  const [titleBlock, setTitleBlock] = useState();
  const [extraCloseModal, setExtraCloseModal] = useState(false);

  const handleClickFinish = () => {
    setActiveModal(true);
    setComments("finish");
    console.log("закрыть окно");

    setExtraCloseModal(true);
    console.log(extraCloseModal);
  };

  const closeModal = (data) => {
    setActiveModal(data);
  };

  const Modal = ({ setActiveModal, titleBlock, openComments }) => {
    const [comments, setComments] = useState(openComments);
    const navigate = useNavigate();

    const modal = {
      position: "fixed",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      width: "100%",
      height: "100%",
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      background: "rgba(0, 0, 0, 0.5)",
    };
    const wrapper = {
      display: "flex",
      width: "694px",
      padding: "64px",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      gap: "32px",
      borderRadius: "10px",
      background: "white",
    };
    const title = {
      marginBottom: "10px",
      fontFamily: "Lato",
      fontSize: "16px",
      fontStyle: "normal",
      fontWeight: "500",
      lineHeight: "24px",
    };
    const subtitle = {
      marginBottom: "30px",
      fontFamily: "Lato",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: "20px",
    };
    const buttonWrapper = {
      width: "100%",
      display: "flex",
      justifyContent: "flex-end",
    };
    const buttonLeft = {
      marginLeft: "20px",
      padding: "10px 20px",
      borderRadius: "10px",
      fontFamily: "Roboto",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "500",
      lineHeight: "20px",
      color: "#156075",
      background: "transparent",
    };
    const buttonRight = {
      marginLeft: "20px",
      padding: "10px 24px",
      borderRadius: "30px",
      fontFamily: "Roboto",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "500",
      lineHeight: "20px",
      color: "#ffffff",
      background: "#156075",
    };
    const titleWrapper = {
      padding: "24px",
      border: "2px solid #4AB9F5",
      borderRadius: "10px",
      marginBottom: "16px",
    };
    const modalContent = {
      width: "100%",
      height: "100%",
    };
    const textareaItem = {
      width: "100%",
      padding: "16px",
      borderRadius: "4px",
      border: "1px solid #D5D5DE",
    };
    const dialog = {
      marginLeft: "20px",
      padding: "10px 20px",
      fontFamily: "Roboto",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "500",
      lineHeight: "20px",
      color: "#156075",
      background: "transparent",
      border: "1px solid #156075",
      borderRadius: "100px",
    };
    const handleClick = () => {
      setActiveModal(false);
      // setComments(true)
      setComments("info");
      document.querySelector("body").classList.remove("is-lock");
      /* вот тут вот */
      setExtraCloseModal(false);
    };
    const handleClickComments = () => {
      // setComments(false)
      setComments("comment");
    };
    const handleClickFinish = () => {
      setActiveModal(false);
      navigate("/");
    };
    const showModalContent = (content) => {
      switch (content) {
        case "info":
          return (
            <div className="modal_wrapper" style={wrapper}>
              <div className="modal_content" style={modalContent}>
                <h2 className="content_title" style={title}>
                  {titleBlock}
                </h2>
                <h3 className="content_subtitle" style={subtitle}>
                  %запрос%
                </h3>
                <div className="content_text">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Distinctio cupiditate dolores earum odit nulla aliquam
                  voluptatem quisquam in vel labore possimus aut, voluptates
                  dicta quasi ullam sapiente quia impedit beatae.
                </div>
              </div>
              <div className="modal_buttons" style={buttonWrapper}>
                <button
                  className="button_comments"
                  style={buttonLeft}
                  onClick={handleClickComments}
                >
                  Оставить комментарий
                </button>
                <button
                  className="button_exite"
                  style={buttonRight}
                  onClick={handleClick}
                >
                  OK
                </button>
              </div>
            </div>
          );
        case "comment":
          return (
            <div className="modal_wrapper" style={wrapper}>
              <div className="modal_content" style={modalContent}>
                <div className="title_wrapper" style={titleWrapper}>
                  <h2 className="content_title" style={title}>
                    {titleBlock}
                  </h2>
                  <h3 className="content_subtitle" style={subtitle}>
                    %запрос%
                  </h3>
                </div>
                <textarea
                  className="content_textarea"
                  name=""
                  id=""
                  cols="30"
                  rows="10"
                  placeholder="%текст_комментария%"
                  style={textareaItem}
                ></textarea>
              </div>
              <div className="modal_buttons" style={buttonWrapper}>
                <button
                  className="button_comments"
                  style={buttonLeft}
                  onClick={handleClick}
                >
                  Отмена
                </button>
                <button
                  className="button_exite"
                  style={buttonRight}
                  onClick={handleClick}
                >
                  Отправить
                </button>
              </div>
            </div>
          );
        case "finish":
          return (
            <div className="modal_wrapper" style={wrapper}>
              <div className="modal_content" style={modalContent}>
                <div className="content_text">
                  {/* Вы уверены, что хотите выйти из учётной записи? */}
                </div>
              </div>
              <div className="modal_buttons" style={buttonWrapper}>
                <button
                  className="button_comments dialog"
                  style={dialog}
                  onClick={handleClickFinish}
                >
                  Да
                </button>
                <button
                  className="button_exite"
                  style={buttonRight}
                  onClick={handleClick}
                >
                  Отмена
                </button>
              </div>
            </div>
          );

        default:
      }
    };
    return (
      <div className="modal" style={modal}>
        {showModalContent(comments)}
      </div>
    );
  };
  return (
    <>
      <SideBarMenu extraCloseModal={extraCloseModal} />
      <div className={css.wrapper}>
        <div className={css.container}>
          <div className={css.dashboardTop}>
            <div className={css.dashboardItemTop}></div>
            <div className={css.dashboardItemTop}></div>
          </div>
          <div className={css.dashboardBottom}></div>
          {activeModal ? (
            <Modal
              setActiveModal={closeModal}
              titleBlock={titleBlock}
              openComments={comments}
            />
          ) : null}
        </div>
        <Plus />
      </div>
    </>
  );
};

export default Administrator;
