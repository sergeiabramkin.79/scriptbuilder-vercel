import { useState } from 'react'
import css from './Home.module.css'
import LinkButton from '../../Components/LinkButton'
import Select from '../../Components/Select/Select';


const Home = () => {
    const [selectedVal, setSelectedVal] = useState('')

    const options = [
        { value: 'Оператор', label: 'Оператор' },
        { value: 'Координатор', label: 'Координатор' },
        { value: 'Администратор', label: 'Администратор' },
        { value: 'Главный администратор', label: 'Главный администратор' }
    ]
    const handleSelect = (e) => {
        e.value === 'Оператор' && setSelectedVal('operator')
        e.value === 'Координатор' && setSelectedVal('coordinator')
        e.value === 'Администратор' && setSelectedVal('administrator')
        e.value === 'Главный администратор' && setSelectedVal('mainadministrator')
    }


    return (
        <div className={css.container} >
            <div className={css.selectMain}>
                <h1 className={css.title}>Добро пожаловать!</h1>
                <p className={css.text}>Для начала работы с приложением вам следует авторизоваться.</p>
                <p className={css.selectTopText}>Я хочу войти в систему как</p>
                <div className={css.itemsWrapper}>
                    <div className={css.selectDiv}>
                        <Select options={options} handleSelect={handleSelect} placeholder="Выбрать" />
                    </div>
                    <LinkButton route={selectedVal} variant={'filled'}>
                        Next
                    </LinkButton>
                </div>
            </div>
        </div>
    )
}

export default Home;