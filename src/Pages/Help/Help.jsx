import css from"./Help.module.css";
import search from "../../Assets/images/search.svg"
import { Link } from "react-router-dom";
import { useState } from "react";
import { cards } from "./cards.js"


const Help = () => {
    const [inputValue, setinputValue] = useState("")
    function handleClick(e) {
        e.preventDefault()
        setinputValue("")
        inputValue !== "" &&  console.log(inputValue)
    }
    function handleChange(e) {
        setinputValue(e.target.value)
    }


    function Card({title,text,link,alt}) {
        return(
            <div className={css.categories_card}>
                <div className={css.categories_card_image}>
                    {/* <img src="" alt={alt} className={css.categories_card_img} width="100%" height="164px"/> */}
                </div>
                <h3 className={css.categories_card_title}>{title}</h3>
                <p className={css.categories_card_text}>{text}</p>
                <Link to={link} className={css.categories_card_link}>Узнать больше</Link>
            </div>
        )
    }


    return ( 
        <div className={css.container}>
            <h1 className={css.title}>Есть вопрос? Мы поможем!</h1>
            <form action="" className={css.form}>
            <label className={css.label_search}>
            <input type="text" className={css.search} placeholder="Поиск..." value={inputValue} onChange={handleChange} id='search'/>
                <button type="submit" className={css.button} onClick={handleClick}>
                    <img src={search} alt="Поиск" className={css.img}/>
                </button>
            </label>
            </form>
            <div className={css.categories}>
                <h2 className={css.categories_title}>Категории</h2>
                <div className={css.categories_cards}>
                    {cards.map((card,index) => {
                        return (
                             <Card title={card.title} text={card.text} link={card.link} key={index} alt={card.title}/>
                        )
                    })}
                </div>
            </div>
        </div>
     );
}

export default Help;