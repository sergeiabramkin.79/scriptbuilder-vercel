import css from './ScriptConstructor.module.css';
import { useState } from 'react';
import SideBarMenu from '../../Components/SideBarMenu/SideBarMenu';
import Toolbar from '../../Components/Toolbar/Toolbar';
import OperatorWindow from '../../Components/OperatorWindow/OperatorWindow';
import ConstructorWindow from '../../Components/ConstructorWindow/ConstructorWindow';
import { ReactFlowProvider }  from 'reactflow';

const ScriptConstructor = () => {
    const [scriptConstructorInner, setScriptConstructorInner] = useState(true);

    const handleClick = (value) => {
        setScriptConstructorInner(value);console.log(value);
    }


    return (
        <>
            <SideBarMenu />
            <Toolbar onClick={handleClick} />

            <div className={css.container}>
                {
                    scriptConstructorInner ?
                        <ReactFlowProvider>
                            <ConstructorWindow />
                        </ReactFlowProvider>
                         : 
                        <OperatorWindow />
                }
            </div>
        </>
    )
}

export default ScriptConstructor;