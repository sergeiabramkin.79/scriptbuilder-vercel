/* eslint-disable import/no-anonymous-default-export */
const data = {
    "uuid": "5dd899bd-3354-4c5c-9be8-ed812836d60e",
    "name": "продажа телевизора",
    "phrases": [
        {
            "uuid": "7cc2d847-4bae-47a7-9a64-41a818e851a0",
            "phrase": "Здравствуйте, вы позвонили в клинику “Здоровье+”, оператор ИМЯ, слушаю вас.",
            "isAllowed": true,
            "note": null,
            "isOperatorPhrase": true
        }
    ],
    "created_at": "2022-01-06T12:00:00Z",
    "last_modified_at": null
}

const client =   {
  uuid: "31a0b872-0dc2-4c9c-9ab3-c70ee2d90936",
  phrase: "Болит живот",
  isAllowed: true,
  note: null,
  isOperatorPhrase: false,
}

/* const data = { script, scriptObject, privet }; */
export default data;
/* Assign object to a variable before exporting as module defaulteslintimport/no-anonymous-default-export */
/* export default {script, scriptObject, privet}; */
