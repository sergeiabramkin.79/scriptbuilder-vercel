// import Table from '../../Components/Table/Table';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import useAxios from '../../hooks/useAxios';
import axios from 'axios';

import ToolbarOperatorMain from '../../Components/ToolbarOperatorMain/ToolbarOperatorMain';
import TableOperator from '../../Components/TableOperator/TableOperator';
import CardsOperator from '../../Components/CardsOperator/CardsOperator';
import css from './Operator.module.css';
import { scriptId } from '../../store/slices/scriptIdSlice';

const Operator = () => {
    const [scripts,setScripts] = useState([]) 
    const [userChoice, setUserChoice] = useState('');
    const [sortButton, setSortButton] = useState(); //true
    const [viewButton, setViewButton] = useState();
    const [sortTableButton, setSortTableButton] = useState(true);
    const [turnSortButton, setTurnSortButton] = useState(false);
    const [valueId, setValueId] = useState('');

    const dispatch = useDispatch()
//    const [error, setError] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
    const [url, setUrl] = useState()
//    const [formData, setFormData] = useState(null)

    // const { response, error } = useAxios(isLoading, url, 'GET', null, null)

//    useEffect(() => {
//     dispatch(scriptId(null))
//     setIsLoading(true)
//     setUrl(' ')
//     let numberScript = 1;
//     // const data = response ? response.data : null
//     console.log(response);
//     // if (response !== null) {console.log('ffff');
//     //     setScripts([...response.data].map((item)=>{ 
//     //         return {...item, number: numberScript++}
//     //     }))
//     //     console.log(response.data);
//     // }
//     if (error) console.error("Произошла ошибка:", error);
// }, [response, error])


// async function fff() {
//     dispatch(scriptId(null))
//     setIsLoading(true)
//     setUrl('/')
//     let numberScript = 1;
//     const data = response ? await response.data : []
//     // let  data;
//     // if(await response.data) {console.log('sss');
//     //     data = response
//     // }
//     console.log(data);
//     // if(await response) console.log('ggggg');
//     // console.log(a);
//     // if(await response) console.log('ffff');
//     // console.log(res);
//     // if (response == null) {console.log('ffff');
//     //     // setScripts([...response.data].map((item)=>{ 
//     //     //     return {...item, number: numberScript++}
//     //     // }))
//         // console.log(data);
//     // }
//     if (error) console.error("Произошла ошибка:", error);
// }
//    useEffect(() => {
//     fff()
// }, [response, error, dispatch])


// const getApiData =  async () => {
//     const response = await axios(
//     //   "https://0vcjfqvote.api.quickmocker.com"
//       "https://api.doscript.pnpl.tech/api/v2/script/all/",
//     // "https://localhost:9090",
//     //   "https://api.doscript.pnpl.tech/api/v2/script/5dd899bd-3354-4c5c-9be8-ed812836d60e",
//       {
//       mode: 'no-cors',
//     method: "GET",
//     // timeout: 1000,
//     // headers: { "Accept": "application/json", "Content-Type": "application/json" },
//     // headers: {
        
//     //     //       // значение этого заголовка обычно ставится автоматически,
//     //     //       // в зависимости от тела запроса
//     //         //   'Content-Type': 'application/json',
//     //     //     'Content-Type': 'text/plain',
//     //     // "Content-Type":"Access-Control-Allow-Origin: *"
//     //     // 'Access-Control-Allow-Origin': 'https://api.doscript.pnpl.tech/api/v2/script/all'
//     //     // 'Access-Control-Allow-Origin': '*'
//     //     // "Access-Control-Allow-Headers: Content-Type",
//     //     // "Content-Type":"application/x-www-form-urlencoded"
//     //         }
//         }
//     )
//     // .then((response) => setArr(response));
//     // ).then((response) => console.log(response.headers));
// //   const data = response ? response.data : null
//     setScripts(response.data);
//     // console.log(response);
//   };




//       useEffect(()=>{
//           getApiData()
//         },[])

// useEffect(() => {
//     const fetchData = async () => {
//       try {
//         const response = await axios('https://api.doscript.pnpl.tech/api/v2/script/all/');
        
//         // Устанавливаем данные и меняем флаг загрузки
//         setScripts(response.data);
//         // setIsLoading(false);
//       } catch (error) {
//         console.error('Error:', error);
//         // Если произошла ошибка, также меняем флаг загрузки
//         // setIsLoading(false);
//       }
//     };

//     // Вызываем функцию загрузки данных
//     fetchData();
//   }, []); // Пустой массив зависимостей означает, что useEffect выполняется только после монтирования компонента

  

    // const scriptsArr = [
    // {
    //   "id": "id354",
    //   "name": "Скрипт 5",
    //   "time": "2023-01-01T20:59:10.000Z",
    //   "data": "2023-06-29T17:06:40.000Z",
    //   "description": "Описание скрипта"
    // }, 
    // {
    //   "id": "id355",
    //   "name": "Скрипт 2",
    //   "time": "2023-05-10T03:10:12.000Z",
    //   "data": "2023-05-28T19:14:30.000Z",
    //   "description": "description"
    // }, 
    // {
    //   "id": "id356",
    //   "name": "Скрипт 7",
    //   "time": "2023-11-20T15:24:48.000Z",
    //   "data": "2023-02-28T07:36:35.000Z",
    //   "description": "description"
    // }, 
    // {
    //   "id": "id357",
    //   "name": "Скрипт 4",
    //   "time": "2023-05-01T18:39:52.000Z",
    //   "data": "2023-04-05T15:44:49.000Z",
    //   "description": "description"
    // }, 
    // {
    //   "id": "id358",
    //   "name": "Скрипт 1",
    //   "time": "2023-03-10T13:12:45.000Z",
    //   "data": "2023-02-12T09:31:17.000Z",
    //   "description": "Описание скрипта"
    // }, 
    // {
    //   "id": "id359",
    //   "name": "Скрипт 6",
    //   "time": "2023-02-15T12:10:37.000Z",
    //   "data": "2023-01-14T01:11:55.000Z",
    //   "description": "description"
    // }, 
    // {
    //   "id": "id360",
    //   "name": "Скрипт 3",
    //   "time": "2023-08-03T14:22:22.000Z",
    //   "data": "2023-07-20T11:40:26.000Z",
    //   "description": "description"
    // }];
   
    // const [scripts,setScripts] = useState(scriptsArr)

    useEffect(() => {
        const fetchData = async () => {
            try {
              const response = await axios('https://api.doscript.pnpl.tech/api/v2/script/all/');
            //   const response = await axios('https://api.doscript.pnpl.tech/api/v2/script/5dd899bd-3354-4c5c-9be8-ed812836d60e');
              dispatch(scriptId(null))
        let numberScript = 1;
        setScripts([...response.data].map((item)=>{ 
                        return {...item, number: numberScript++}
                    }).reverse())
              // Устанавливаем данные и меняем флаг загрузки
            //   setScripts(response.data);
              // setIsLoading(false);
            } catch (error) {
              console.error('Error:', error);
              // Если произошла ошибка, также меняем флаг загрузки
              // setIsLoading(false);
            }
          };
      
          // Вызываем функцию загрузки данных
          fetchData();
        
        },[])
        
        // console.log(scripts[0]);
    const sortScripts = (userChoiceSelect,buttonSort) => {
        setUserChoice(userChoiceSelect)
        setSortButton(buttonSort)
    }
    const viewScripts = (buttonView) => {
        setViewButton(buttonView)
    }
    const turnSortButtonInit = (init) => {
        setTurnSortButton(init)
    }
    const sortTableClick = (tableName) => {
        if(tableName === "number") {
            sortTableButton ? 
            setScripts([...scripts].sort((a, b) => a.number > b.number ? -1 : 1)) :
            setScripts([...scripts].sort((a, b) => a.number < b.number ? -1 : 1))
            setSortTableButton(!sortTableButton)
            // console.log('number');
            // console.log('creating',sortTableButton,scripts);
        } else if(tableName === "name") {
            sortTableButton ? 
            setScripts([...scripts].sort((a, b) => a.name.toLowerCase() > b.name.toLowerCase() ? -1 : 1)) :
            setScripts([...scripts].sort((a, b) => a.name.toLowerCase() < b.name.toLowerCase() ? -1 : 1))
            setSortTableButton(!sortTableButton)
            // console.log('name');
        } else if(tableName === "changing") {
            sortTableButton ? setScripts([...scripts].sort((a, b) => new Date(a.last_modified_at).getTime() > new Date(b.last_modified_at).getTime() ? -1 : 1)) :
                     setScripts([...scripts].sort((a, b) => new Date(a.last_modified_at).getTime() < new Date(b.last_modified_at).getTime() ? -1 : 1))
                     setSortTableButton(!sortTableButton)
                    //  console.log('changing');
        } else if(tableName === "creating") {
            sortTableButton ? setScripts([...scripts].sort((a, b) => new Date(a.created_at
                ).getTime() > new Date(b.created_at
                    ).getTime() ? -1 : 1)) :
                         setScripts([...scripts].sort((a, b) => new Date(a.created_at
                            ).getTime() < new Date(b.created_at
                                ).getTime() ? -1 : 1))
                     setSortTableButton(!sortTableButton)
                    //  console.log('creating',sortTableButton,scripts);
        }
    }

    const sortReverse = () => {
         setScripts([...scripts].reverse())
    }
    const sortTimeOfCreation = (sortButton) => {console.log('creating');
        sortButton ? setScripts([...scripts].sort((a, b) => new Date(a.created_at).getTime() > new Date(b.created_at).getTime() ? -1 : 1)) :
        setScripts([...scripts].sort((a, b) => new Date(a.created_at).getTime() < new Date(b.created_at).getTime() ? -1 : 1))
    }
    const sortName = (sortButton) => {
        sortButton ? setScripts([...scripts].sort((a, b) => a.name.toLowerCase() > b.name.toLowerCase() ? -1 : 1)) :
                     setScripts([...scripts].sort((a, b) => a.name.toLowerCase() < b.name.toLowerCase() ? -1 : 1))
    }
    const sortLastChanging = (sortButton) => {
        sortButton ? setScripts([...scripts].sort((a, b) => new Date(a.last_modified_at).getTime() > new Date(b.last_modified_at).getTime() ? -1 : 1)) :
        setScripts([...scripts].sort((a, b) => new Date(a.last_modified_at).getTime() < new Date(b.last_modified_at).getTime() ? -1 : 1))
    }

    useEffect(
        () => {
            userChoice === '' && sortButton !== undefined && sortReverse()
            userChoice === 'Дата создания' && sortTimeOfCreation(sortButton)
            userChoice === 'Название' && sortName(sortButton)
            userChoice === 'Последнее изменение' && sortLastChanging(sortButton)
        },[sortButton,viewButton]
    )

    return (
        <div className={css.wrapper}>
            <div className={css.container}>
                <ToolbarOperatorMain 
                    sortScripts={sortScripts} 
                    viewScripts={viewScripts}
                    turnSortButtonInit={turnSortButtonInit}
                    turnSortButton={turnSortButton}
                    valueId={valueId}
                />
                <div className={css.content}>
                   { viewButton ?  <CardsOperator 
                                        scriptsArray={scripts}
                                   /> : 
                                   <TableOperator 
                                        valueId={valueId}
                                        setValueId={setValueId}
                                        scriptsArray={scripts}
                                        sortTableClick={sortTableClick}
                                        turnSortButtonInit={turnSortButtonInit}
                                   />
                    }
                </div>
            </div>
        </div>
    )
}

export default Operator;
