// slices/blocksSlice.js
import { createSlice } from "@reduxjs/toolkit";

const blocksSlice = createSlice({
  name: "blocks",
  initialState: {
    blocks: [
      {
        uuid: "253b7550-53e6-433c-ba87-fb914a71b489",
        name: "запись к врачу",
        phrases: [],
        created_at: "2022-01-05T12:00:00Z",
        last_modified_at: null,
      },
      {
        uuid: "5dd899bd-3354-4c5c-9be8-ed812836d60e",
        name: "продажа телевизора",
        phrases: [
          {
            uuid: "7cc2d847-4bae-47a7-9a64-41a818e851a0",
            phrase:
              "Здравствуйте, вы позвонили в клинику “Здоровье+”, оператор ИМЯ, слушаю вас.",
            isAllowed: true,
            note: null,
            isOperatorPhrase: true,
          },
        ],
        created_at: "2022-01-06T12:00:00Z",
        last_modified_at: null,
      },
      {
        uuid: "31a0b872-0dc2-4c9c-9ab3-c70ee2d90936",
        phrase: "Болит живот",
        isAllowed: true,
        note: null,
        isOperatorPhrase: false,
      },
      [
        {
          uuid: "1d406706-24c4-4982-9b03-a21f77c22a54",
          phrase: "Хорошо, как я могу к вам обращаться?",
          isAllowed: true,
          note: null,
          isOperatorPhrase: true,
        },
      ],
    ],
  },
  reducers: {
    updateBlocks: (state, action) => {
      const { block1, block2, block3 } = action.payload;
      state.block1.content = block1;
      state.block2.content = block2;
      state.block3.content = block3;
    },
  },
});

export const { updateBlocks } = blocksSlice.actions;
export default blocksSlice.reducer;
