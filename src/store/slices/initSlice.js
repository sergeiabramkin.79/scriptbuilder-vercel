// Ваш слайс (initSlice.js)
import { createSlice } from '@reduxjs/toolkit';

const initSlice = createSlice({
  name: 'initSlice',
  initialState: {
    data: null,
    clientReplics: [], // Добавляем массив для реплик клиента
    operatorReplics: [], // Добавляем массив для реплик оператора
    loading: false,
    error: null,
  },
  reducers: {
    fetchDataRequest: (state) => {
      state.loading = true;
      state.error = null;
    },
    fetchDataSuccess: (state, action) => {
      state.loading = false;
      state.data = action.payload;
    },
    fetchDataFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
    fetchClientReplic: (state, action) => {
      state.clientReplics = action.payload;
    },
    fetchOperatorReplic: (state, action) => {
      state.operatorReplics = action.payload;
    },
  },
});

export const {
  fetchDataRequest,
  fetchDataSuccess,
  fetchDataFailure,
  fetchClientReplic,
  fetchOperatorReplic,
} = initSlice.actions;
export default initSlice.reducer;
