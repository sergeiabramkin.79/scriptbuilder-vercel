import { createSlice } from '@reduxjs/toolkit';
import { applyNodeChanges } from 'reactflow';


// const initialNodes = [
//     { id: '1', sourcePosition: 'right', targetPosition: 'left', position: { x: 0, y: 0 }, data: { label: 1 }, type: 'textUpdater', parentNode: null, dChildren: [] },
//     { id: '2', sourcePosition: 'right', targetPosition: 'left', position: { x: 240, y: 0 }, data: { label: 2 }, type: 'textUpdater', parentNode: null, dChildren: [] },
// ]

const initialState = []

const nodesSlice = createSlice({
    name: 'nodes',
    initialState: initialState,

    reducers: {
        updateNodes(state, action) {
            return [...state, action.payload]
        },
        updateNodeData(state, action) {
            console.log('payload', action.payload)
            return state.map(item => {
                if (item.id === action.payload.id) {
                    item = {
                        ...item,
                        data: {
                            ...item.data,
                            label: action.payload.data.label,
                            content: action.payload.data.content
                        }
                    }
                }
                return item
            })
        },
        updateNodesOnChange(state, action) {
            return applyNodeChanges(action.payload, state)
        },
        toggleIsEditActive(state, action) {
            if (!action.payload) {
                return state.map(item => {
                    item = {
                        ...item,
                        data: { ...item.data, isEditActive: false }
                    }
                    return item
                })
            } else {
                return state.map(item => {
                    if (item.id === action.payload) {
                        item = {
                            ...item,
                            data: { ...item.data, isEditActive: !item.data.isEditActive }
                        }
                    }
                    return item
                })
            }
        },
        clearState(state) {
            return initialState
        }
    },

});

export const { updateNodes, updateNodeData, updateNodesOnChange, clearState, toggleIsEditActive } = nodesSlice.actions;

export default nodesSlice.reducer;
