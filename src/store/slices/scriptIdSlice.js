import { createSlice } from '@reduxjs/toolkit';

const scriptIdSlice = createSlice({
    name: 'scriptId',
    initialState: null,
    reducers: {
        scriptId(state, action) {
            return action.payload
        },
    }
})

export const { scriptId } = scriptIdSlice.actions;
export default scriptIdSlice.reducer