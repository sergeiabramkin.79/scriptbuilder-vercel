import { createSlice } from '@reduxjs/toolkit'
import Cookies from "js-cookie";

let initialState;

if (Cookies.get('userName')) {
    initialState = {
        authLevel: 1,
        userName: Cookies.get('userName'),
        accessToken: Cookies.get('accessToken'),
        refreshToken: Cookies.get('refreshToken'),
    }
} else initialState = null

const userSlice = createSlice({
    name: 'user',
    initialState: initialState ||
    {
        authLevel: null,
        userName: '',
        accessToken: '',
        refreshToken: '',
    },

    reducers: {
        logIn(state, action) {
            console.log('payload', action.payload);
            return action.payload
        }
    },
});

export const { logIn } = userSlice.actions;

export default userSlice.reducer;