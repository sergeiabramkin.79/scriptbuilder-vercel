import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./slices/userSlice";
import nodesReducer from "./slices/nodesSlice";
import edgesReducer from "./slices/edgesSlice";
import treeReducer from "./slices/treeSlice";
import scriptIdReducer from "./slices/scriptIdSlice";

import blocksReducer from './slices/dialogSlice';
import initReducer from './slices/initSlice';


export const store = configureStore({
  reducer: {
    user: userReducer,
    nodes: nodesReducer,
    edges: edgesReducer,
    tree: treeReducer,
    scriptId: scriptIdReducer,
    blocks: blocksReducer,
    init: initReducer,
  },

})
