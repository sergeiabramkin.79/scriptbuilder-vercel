import { useState, useRef, useEffect } from "react";
import css from "./SideBarMenu.module.css";
// import iconExit from "../../Assets/images/icon-exit.svg";
import ControlPanel from "../../Assets/images/control-panel.svg";
import Pencil from "../../Assets/images/pencil.svg";
import Comment from "../../Assets/images/comment.svg";
import { Link, useLocation } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { clearState } from '../../store/slices/nodesSlice'
import ModalCloseAdmin from '../ModalCloseAdmin/ModalCloseAdmin'

const SideBarMenu = ({extraCloseModal}) => {
    useEffect(() => {
        if (extraCloseModal) {
          setIsOpen(false)
        }
        if(!extraCloseModal){
            setIsOpen(true)
        }
      }, [extraCloseModal]);
    const [isOpen,setIsOpen] = useState(true)
    const [sidebarWidth, setSidebarWidth] = useState()
    const [linkActive, setLinkActive] = useState()

    const dispatch = useDispatch();
    const { pathname } = useLocation()

    useEffect(() => {
        setLinkActive(pathname)
    },[pathname])
    
    const open = {
        left: 0
    }
    const close = {
        left: -`${sidebarWidth}`
    }
    const sidebarwidth = useRef(null)

    const handleclick = () => setIsOpen(!isOpen)
    // const handleClickExit = () => {
    //     console.log('zzzz');
    // }
    useEffect(() => {
        setSidebarWidth(sidebarwidth.current.offsetWidth)
        }, [sidebarWidth]);


    return (
        <div className={css.sidebarmenu} style={isOpen ? open : close}>
            <div ref={sidebarwidth} className={css.wrapper}>
                <ul className={css.list}>
                    <li className={css.item}>
                        <img src={ControlPanel} alt="Control panel" className={css.itemImg}/>
                        <Link to='/administrator' className={linkActive === '/administrator' ? `${css.linkActive}` : null}>Главная</Link>
                    </li>
                    <li className={css.item} onClick={() => dispatch(clearState())}> 
                        <img src={Pencil} alt="Pencil" className={css.itemImg}/>
                        <Link to='/constructor'  className={linkActive === '/constructor' ? `${css.linkActive}` : null}>Конструктор скриптов</Link>
                    </li>
                    <li className={css.item}>
                        <Link to='/administrator/allscripts'  className={linkActive === '/administrator/allscripts' ? `${css.linkActive}` : null}>Все скрипты</Link>
                    </li>
                    <li className={css.item}>
                        <Link to='/administrator/templates'  className={linkActive === '/administrator/templates' ? `${css.linkActive}` : null}>Шаблоны</Link>
                    </li>
                    <li className={css.item}>
                        <Link to='/administrator/drafts'  className={linkActive === '/administrator/drafts' ? `${css.linkActive}` : null}>Черновики</Link>
                    </li>
                    <li className={css.item}>
                        <img src={Comment} alt="Comment" className={css.itemImg}/>
                        <Link to='/administrator/comments'  className={linkActive === '/administrator/comments' ? `${css.linkActive}` : null}>Комментарии оператора</Link>
                    </li>
                </ul>
                <div>
                    <ModalCloseAdmin/>
                </div>
            </div>
            <span className={!isOpen ? `${css.sidebarbutton}` : `${css.sidebarbutton} ${css.open}`} onClick={handleclick}></span>
        </div>
     );
}

export default SideBarMenu;