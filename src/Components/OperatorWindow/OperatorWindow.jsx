import css from './OperatorWindow.module.css'

function OperatorWindow() {

    const itemClass = 
        { operator: `${css.item_text} ${css.operator}`,
          client: `${css.item_text} ${css.client}`,
          forbidden: `${css.item_text} ${css.operator} ${css.forbidden}`
        }

    const clientReplicas = [
        {
            text: "Хочу записаться к n-специалисту",
            className: itemClass.client
        },
        // {text: "Хочу спросить"}
    ]

    const ClientReplicas = ({className,text}) => {
        return(
                <p className={className}>{text}</p>
        )
    }

    const operatorReplicas = [
        {
            text:"Давайте подберем специалиста и согласуем дату/время.",
            className: itemClass.operator
        },
        {
            text:"Уточните пожалуйста симптомы? Какие у вас жалобы?",
            className: itemClass.operator
        },
        {
            text:"У нас всё расписано на ближайшие две недели.",
            className: itemClass.forbidden
        },
        {
            text:"Что у вас болит? Где болит?",
            className: itemClass.forbidden
        },
    ]

    const OperatorReplicas = ({text,className}) => {
        return(
            <p className={className}>{text}</p>
        )
    }

    const templates = [
        {title: "Приветствие"},
        {title: "Выявление потребностей"},
        {title: "Продажа"},
        {title: "Работа с возражениями"},
        {title: "Подбор специалиста"},
        {title: "Информирование о  порядке подготовки к приёму"},
    ]

    const Template = ({title}) => {
        return(
            <div className={css.template}>
                <h2 className={css.template_title}>
                    {title}
                </h2>
            </div>
        )
    }

    const talk = [
        {
            title: "Приветствие",
            text: "Reply",
            className: itemClass.client
        },
        {
            title: "Базовое приветствие",
            text: "Добрый день, меня зовут %имярек%.",
            className: itemClass.operator
        },
        {
            title: "Выявление потребностей",
            text: "Чем я могу вам помочь?",
            className: itemClass.operator
        },
        {
            title: "Запрос",
            text: "Хочу записаться к n-специалисту",
            className: itemClass.client
        },
    ]

    const Talk = ({title,text,className}) => {
        return(
            <div className={`${css.item_inner} ${className}`}>
                <h2 className={`${css.item_text}`}>{title}</h2>
                <p className={`${css.item_text} ${css.small}`}>{text}</p>
            </div>
        )
    }    

    return ( 
        <div className={css.container}>
             <div className={css.item}>
               <h2 className={css.title}>Реплики клиента</h2>
                {clientReplicas.map((item,index) => {
                return(
                    <ClientReplicas text={item.text} key={index} className={item.className}/>
                )
            })}
            </div> 
            <div className={css.item}>
                <div className={css.item_top}>
                <h2 className={css.title}>Реплики оператора</h2>
                {operatorReplicas.map((item,index) => {
                    return(
                        <OperatorReplicas text={item.text} key={index} className={item.className}/>
                    )
                })}
                </div>
                <div className={css.item_bottom}>
                <h2 className={css.title}>Шаблоны</h2>
                {templates.map((item,index)=> {
                    return(
                        <Template title={item.title} key={index}/>
                    )
                })}
                </div>
            </div>
            <div className={css.item}>
                    {talk.map((item,index) => {
                        return(
                            <Talk title={item.title}  text={item.text} key={index} className={item.className}/>
                        )
                    })}
            </div>
        </div>
     );
}

export default OperatorWindow;