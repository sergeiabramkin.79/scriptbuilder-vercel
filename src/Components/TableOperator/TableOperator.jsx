import css from "./TableOperator.module.css";
import { scriptId } from "../../store/slices/scriptIdSlice";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import sortArrows from "../../Assets/images/icon-sort-arrows.svg";

function TableOperator({
  valueId,
  setValueId,
  scriptsArray,
  sortTableClick,
  turnSortButtonInit,
}) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const idScript = useSelector(state => state.scriptId);

  const handleClick = (e) => {
    if (
      e.target.closest("th") &&
      e.target.closest("th").hasAttribute("data-name")
    ) {
      sortTableClick(e.target.closest("th").getAttribute("data-name"), true);
      turnSortButtonInit(true);
    }

  };
  const handleChange = (e) => {
    // setValueId(e.target.value);
    dispatch(scriptId(e.target.value));
  };
  const handleDoubleClick = (e) => {
    let idScript = e.target.closest("tr").querySelector("input").value;
    dispatch(scriptId(idScript));
    navigate("/dialog");
  };

  return (
    <table className={css.table} onClick={handleClick}>
      <thead>
        <tr>
          <th></th>
          <th data-name="number">
            №
            <img
              src={sortArrows}
              alt="icon-arrows"
              className={css.sortArrows}
            />
          </th>
          <th data-name="name">
            Название
            <img
              src={sortArrows}
              alt="icon-arrows"
              className={css.sortArrows}
            />
          </th>
          <th data-name="changing">
            Время последнего изменения
            <img
              src={sortArrows}
              alt="icon-arrows"
              className={css.sortArrows}
            />
          </th>
          <th data-name="creating">
            Дата создания
            <img
              src={sortArrows}
              alt="icon-arrows"
              className={css.sortArrows}
            />
          </th>
          <th></th>
        </tr>
      </thead>
      <tbody onDoubleClick={handleDoubleClick}>
        {
        scriptsArray.map((item, index) => {
          return (
            <tr key={index}>
              <td>
                <input
                  type="radio"
                  name="radio-button"
                  onChange={handleChange}
                  // value={item.id}
                  value={item.uuid}
                  checked={
                    // valueId === item.id}
                  // idScript === item.id}
                  idScript === item.uuid}
                />
              </td>
              <td>{item.number}</td>
              <td>{item.name}</td>
              {/* <td>{new Date(item.time).toLocaleString()}</td>
              <td>{new Date(item.data).toLocaleString()}</td> */}
              <td>{new Date(item.last_modified_at).toLocaleString()}</td>
              <td>{new Date(item.created_at).toLocaleString()}</td>
              <td></td>
            </tr>
          );
        })
        }
      </tbody>      
    </table>
  );
}

export default TableOperator;
