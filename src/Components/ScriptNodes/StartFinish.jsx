import { Handle, Position } from 'reactflow';


const styles = {
    width: '174px',
    height: '40px',
    borderRadius: '100px',
    border: '1px solid #BF76ED',
    backgroundColor: '#F5E5FF',
    display: 'flex',
    alignItems: 'center'
};

const stylesTitle = {
    fontFamily: 'Roboto',
    fontSize: '0.75rem',
    fontWeight: '600',
    width: '100%',
    textAlign: 'center',
};

const StartFinish = (props) => {

    return (
        <>
            <Handle type="target" position={Position.Left} />
            <div className='dragHandle' style={styles}>
                <h4 style={stylesTitle}>{props.data.label}</h4>
            </div>
            <Handle type="source" position={Position.Right} id="a" />
        </>
    );
}

export default StartFinish