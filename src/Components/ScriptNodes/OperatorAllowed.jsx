import { useEffect, useState } from 'react'
import { Handle, Position } from 'reactflow';
import { shallowEqual, useSelector, useDispatch } from 'react-redux';
import { updateNodeData, toggleIsEditActive } from '../../store/slices/nodesSlice'
import { updateTreeNodeData } from '../../store/slices/treeSlice'


const styles = {
    maxWidth: '160px',
    minWidth: '100px',
    height: 'fitContent',
    borderRadius: '10px',
    border: '1px solid #56C24E',
    backgroundColor: '#E6FFE4',
    padding: '4px',
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    gap: '4px',
};

const stylesTitle = {
    fontFamily: 'Roboto',
    fontSize: '0.75rem',
    fontWeight: '600',
    width: '100%',
    textAlign: 'center',
    borderRadius: '10px',
};

const stylesContent = {
    fontFamily: 'Roboto',
    fontSize: '0.625rem',
    fontWeight: '400',
    borderRadius: '10px',
};

const stylesTitleEdit = {
    fontFamily: 'Roboto',
    fontSize: '0.75rem',
    fontWeight: '600',
    outline: '0',
    border: '1px solid black',
    backgroundColor: 'transparent',
    width: '100%',
    borderRadius: '10px',
    padding: '0 8px',
};

const stylesContentEdit = {
    fontFamily: 'Roboto',
    fontSize: '0.625rem',
    fontWeight: '400',
    outline: '0',
    border: '1px solid black',
    backgroundColor: 'transparent',
    minWidth: '80%',
    minHeight: '90px',
    verticalAlign: 'top',
    resize: 'none',
    borderRadius: '10px',
    padding: '0 8px',
};

const OperatorAllowed = (props) => {
    const nodes = useSelector(state => state.nodes, shallowEqual)
    const treeNodes = useSelector(state => state.tree.nodes, shallowEqual)
    const dispatch = useDispatch()

    const curNode = nodes.find(item => item.id === props.id)
    const curTreeNode = treeNodes.find(item => item.uuid === props.id)

    const [label, setLabel] = useState(props.data.label)
    const [content, setcontent] = useState(props.data.content)

    const onChange = (e) => {
        e.target.getAttribute('data-id') === 'label' && setLabel(e.target.value)
        e.target.getAttribute('data-id') === 'content' && setcontent(e.target.value)
    }

    const handleDoubleClick = () => {
        dispatch(toggleIsEditActive(props.id))
    }

    useEffect(() => {
        const clonedCurNode = JSON.parse(JSON.stringify(curNode))
        const clonedCurTreeNode = JSON.parse(JSON.stringify(curTreeNode))
        clonedCurNode.data.label = label
        clonedCurNode.data.content = content
        clonedCurTreeNode.data.label = label
        clonedCurTreeNode.data.content = content

        dispatch(updateNodeData(clonedCurNode))
        dispatch(updateTreeNodeData(clonedCurTreeNode))
    }, [label, content])


    return (
        <>
            <Handle type="target" position={Position.Left} />
            <div style={styles} onDoubleClick={handleDoubleClick}>
                {
                    !props.data.isEditActive ?
                        <div className='dragHandle'>
                            <h4 style={stylesTitle}>{props.data.label}</h4>
                            <p style={stylesContent}>{props.data.content}</p>
                        </div>
                        :
                        <>
                            <input style={stylesTitleEdit} type="text" value={label} data-id={'label'} onChange={(e) => onChange(e)} />
                            <textarea style={stylesContentEdit} value={content} data-id={'content'} onChange={(e) => onChange(e)} />
                        </>
                }
            </div>
            <Handle type="source" position={Position.Right} id="a" />
        </>
    );
}

export default OperatorAllowed