import css from './Button.module.css';


export const Button = ({ children, disabled, handleClick, type, variant, id }) => {
    const variants = {
        filled: `${css.btn} ${css.btnFilled}`,
		edition: `${css.btn} ${css.btnEdition}`,
		editionActive : `${css.btn} ${css.btnEdition} ${css.active}`,
		viewing: `${css.btn} ${css.btnViewing}`,
		viewingActive : `${css.btn} ${css.btnViewing} ${css.active}`,
		approve: `${css.btn} ${css.btnApprove}`,
		save: `${css.btn} ${css.btnSave}`,
        btnTextSmall: `${css.btn} ${css.btnTextSmall}`,
        close: `${css.btn} ${css.btnClose}`,
		searchOperator: `${css.btnSearchOperator}`,
		sortOperator: `${css.btnSortOperator}`,
		sortOperatorActive: `${css.btnSortOperator} ${css.active}`,
		viewOperator: `${css.btnViewOperator}`,
		viewOperatorActive: `${css.btnViewOperator} ${css.active}`,
		newDialogOperator: `${css.btnDialogOperator}`,
		exitAdmin: `${css.btnExitAdmin}`
    }

    
	return (
		<button
			type={type}
			disabled={disabled}
            className={variants[`${variant}`]}
			onClick={handleClick}
			id={id}
		>
			{children}
		</button>
	)
}

