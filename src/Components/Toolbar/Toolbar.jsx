import { useState } from "react";
import css from "./Toolbar.module.css"
import { useLocation } from "react-router-dom";
import { Button } from "../Button/Button";
import checkmark from "../../Assets/images/checkmark.svg"

const Toolbar = ({onClick}) => {
    const[btnactiveEdition, setBtnactiveEdition] = useState('editionActive')
    const [btnactiveViewing, setBtnactiveViewing] = useState('viewing')
    const [titleScript, setTitleScript] = useState('')
    const { pathname } =useLocation()

    const handleClick = (value) => {
        onClick(value)
    }

    const handleClickEdition = () => {
        setBtnactiveEdition('editionActive')
        setBtnactiveViewing('viewing')
        handleClick(true)
    }
    const handleClickViewing = () => {
        setBtnactiveEdition('edition')
        setBtnactiveViewing('viewingActive')
        handleClick(false)
     }
     const handleChange = (e) => {
        setTitleScript(e.target.value)
     }
     const handleClickSave = () => {
        console.log('gggggg');
     }
    return ( 
        <div className={css.wrapper}>
        <div className={css.container}>
            <div className={css.item_left}>
                {/* <h2 className={css.title}>%название скрипта%</h2> */}
                <input type="text" className={css.title} onChange={handleChange} value={titleScript} placeholder="%название скрипта%"/>
                <div className={css.segmentes_button_wrapper}>
                <Button variant={btnactiveEdition} handleClick={handleClickEdition} id="edition">
                    {
                        btnactiveEdition === 'editionActive' ? <img className={css.checkmark} src={checkmark} alt='checkmark'/> : null
                    }
                      Редактирование
                </Button>
                <Button variant={btnactiveViewing} handleClick={handleClickViewing} id="viewing">
                {
                        btnactiveViewing === 'viewingActive' ? <img className={css.checkmark} src={checkmark} alt='checkmark'/> : null
                    }
                      Просмотр
                </Button>
                </div>
            </div>
            <div className={css.item_right}>
            <Button variant={'save'} handleClick={handleClickSave}>Сохранить</Button>
            <Button variant={'approve'}>Согласовать</Button>
            </div>
        </div>
        </div>
     );
}

export default Toolbar;