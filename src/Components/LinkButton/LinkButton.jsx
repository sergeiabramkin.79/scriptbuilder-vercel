import css from './LinkButton.module.css';
import { Link } from 'react-router-dom'


const LinkButton = ({ children, route, variant }) => {
    const variants = {
        filled: `${css.btn} ${css.btnFilled}`,
        largeFAB: `${css.btn} ${css.btnLargeFAB}`,
        btnExit: `${css.btn} ${css.btnExit}`,
        newDialog: `${css.btn} ${css.btnNewDialog}`,
        newDialogActive: `${css.btn} ${css.btnNewDialog} ${css.active}`
    }


	return (
        <div className={variants[`${variant}`]} >                       
            <Link to={`/${route}`} >{children}</Link>
        </div>
		
	)
}

export default LinkButton