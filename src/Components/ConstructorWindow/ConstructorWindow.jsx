// import css from './ConstructorWindow.module.css'
import { useCallback, useEffect, useRef, useState, useMemo } from 'react';
import { v4 as uuidv4 } from 'uuid';
import ReactFlow, { } from 'reactflow';
import { shallowEqual, useSelector, useDispatch } from 'react-redux';
import { updateNodes, updateNodesOnChange, toggleIsEditActive } from '../../store/slices/nodesSlice'
import { updateEdges, updateEdgesOnChange } from '../../store/slices/edgesSlice'
import { addTreeNode, updateSpecificTreeNode } from '../../store/slices/treeSlice'
import 'reactflow/dist/style.css';
import StartFinish from '../ScriptNodes/StartFinish'
import Client from '../ScriptNodes/Client'
import OperatorAllowed from '../ScriptNodes/OperatorAllowed'
import OperatorNotAllowed from '../ScriptNodes/OperatorNotAllowed'
import AssetsBar from '../AssetsBar/';


const ConstructorWindow = () => {
    const nodeTypes = useMemo(() => (
        {
            startFinish: StartFinish,
            client: Client,
            operatorAllowed: OperatorAllowed,
            operatorNotAllowed: OperatorNotAllowed
        }
    ), []);

    const reactFlowWrapper = useRef(null);
    const [reactFlowInstance, setReactFlowInstance] = useState(null);

    const dispatch = useDispatch();

    const nodes = useSelector(state => state.nodes, shallowEqual)
    const edges = useSelector(state => state.edges, shallowEqual)
    const tree = useSelector(state => state.tree, shallowEqual)

    const onNodesChange = useCallback(
        changes =>  dispatch(updateNodesOnChange(changes)),
        [dispatch]
    );

    const onEdgesChange = useCallback(
        changes => dispatch(updateEdgesOnChange(changes)),
        [dispatch]
    );

    const onDragOver = useCallback((event) => {
        event.preventDefault();
        event.dataTransfer.dropEffect = 'move';
    }, []);

    const onDrop = useCallback(
        (event) => {
            event.preventDefault();
            const type = event.dataTransfer.getData('application/reactflow');
            console.log('node type', type)

            //   check if the dropped element is valid
            if (typeof type === 'undefined' || !type) {
                return;
            }

            const position = reactFlowInstance.screenToFlowPosition({
                x: event.clientX,
                y: event.clientY,
            });

            const id = uuidv4();

            const newNode = {
                id: id,
                type,
                position,
                data: {
                    label: type !== 'startFinish' ? 'Title' : 'Start / Finish',
                    content: type !== 'startFinish' && 'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet',
                    isEditActive: false
                },
                sourcePosition: 'right',
                targetPosition: 'left',
                dragHandle: '.dragHandle'
            };

            const newTreeNode = {
                uuid: id,
                type,
                data: {
                    label: type !== 'startFinish' ? 'Title' : 'Start',
                    content: type !== 'startFinish' && 'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet'
                },
                // parentId: null,
                childrenIds: [],
                isOperatorPhrase: type === 'operatorAllowed' || type === 'operatorNotAllowed' ? true : false,
                status: '',
                note: '',
                isRoot: type !== 'startFinish' ? false : true,
            }

            dispatch(updateNodes(newNode))
            dispatch(addTreeNode(newTreeNode))
        },
        [reactFlowInstance, dispatch],
    );

    const onConnect = useCallback((params) => {
        const newEdge = {
            id: `e${params.source}-${params.target}`,
            type: 'smoothstep',
            source: params.source,
            target: params.target 
        }

        if (edges.find(item => item.id === newEdge.id)) return

        const parentNode = { ...tree.nodes.find(item => item.uuid === params.source) }
        // const childNode = { ...tree.nodes.find(item => item.id === params.target) }
        console.log('parentNode', parentNode)
        parentNode.childrenIds = [...parentNode.childrenIds, params.target]
        // childNode.parentNode = params.source

        // const updatedTree = tree.nodes.map(item => {
        //     if (item.id === parentNode.id) {
        //         item = parentNode
        //     } else if (item.id === childNode.id) {
        //         item = childNode
        //     }
        //     return item
        // })

        console.log('parentNode', parentNode)
        // console.log('childNode', childNode)

        // dispatch(updateTree(updatedTree))
        dispatch(updateSpecificTreeNode(parentNode))
        // dispatch(updateSpecificTreeNode(childNode))
        dispatch(updateEdges(newEdge))
    }, [tree.nodes, edges, dispatch]);

    const handleClickOutside = () => {
        const hasActive = nodes.some(item => item.data.isEditActive)
        hasActive && dispatch(toggleIsEditActive(null))
    }

    useEffect(() => {
        console.log('nodes', nodes)
        console.log('edges', edges)
        console.log('tree', tree)
    }, [nodes, edges, tree])


    return (
        <>
            <div style={{ width: '100vw', height: '100vh' }} ref={reactFlowWrapper}>
                <ReactFlow
                    nodes={nodes}
                    edges={edges}
                    onNodesChange={onNodesChange}
                    onEdgesChange={onEdgesChange}
                    onConnect={onConnect}
                    nodeTypes={nodeTypes}
                    onInit={setReactFlowInstance}
                    onDrop={onDrop}
                    onDragOver={onDragOver}
                    onPaneClick={handleClickOutside}
                />
            </div>
            <AssetsBar />
        </>
    );
}

export default ConstructorWindow;