export const styles = {
    container: (baseStyles,state) => ({
        ...baseStyles,
        width: '182px',
    }),
    control: (baseStyles, state) => ({
        ...baseStyles,
        maxWidth: '182px',
        width: '100%',
        border: '2px solid #ABABB5',
        boxShadow: 'none',
        "&:hover": {
            outline: 'none',
            cursor: 'pointer',
        },
    }),
    dropdownIndicator: (base, state) => ({
    ...base,
    color: '#111111',
    fontSize: '50px',
    transition: 'all .2s ease',
    transform: state.selectProps.menuIsOpen && "rotate(180deg)"
  }),
  indicatorSeparator: (base, state) => ({
    ...base,
    display: 'none'
  }),
  placeholder: (baseStyles, state) => ({
    ...baseStyles,
    fontFamily: 'Manrope, san-serif',
    fontSize: '18px',
    fontStyle: 'normal',
    fontWeight: '500',
    lineHeight: '1.44',
    color: '#111111',
}),
option: (baseStyles, state) => ({
  ...baseStyles,
  padding: '8px 16px',
  fontFamily: 'Manrope, san-serif',
  fontSize: '18px',
  fontStyle: 'normal',
  fontWeight: '500',
  lineHeight: '1.44',
  color: '#111111',
  border: '2px solid transparent',
  backgroundColor: 'transparent',
  "&:hover": {
    backgroundColor: 'transparent',
    border: "2px solid #156075",
},
}),
singleValue: (base, state) => ({
  ...base,
  fontFamily: 'Manrope, san-serif',
  fontSize: '18px',
  fontStyle: 'normal',
  fontWeight: '500',
  lineHeight: '1.44',
  color: '#111111',
}),
}