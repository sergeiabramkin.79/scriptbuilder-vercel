import ReactSelect from 'react-select'

import css from "./ToolbarOperatorMain.module.css";
import iconExit from "../../Assets/images/icon-exit.svg";
import iconSort from "../../Assets/images/icon-sort.svg";
import iconViewCard from "../../Assets/images/icon-view-card.svg";
import iconViewTable from "../../Assets/images/icon-view-table.svg";
import iconCross from "../../Assets/images/icon-cross-button.svg";
import search from "../../Assets/images/search.svg"


import {styles} from "./selectConfig";
import LinkButton from '../LinkButton/LinkButton';
import { Button } from '../Button/Button';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

function ToolbarOperatorMain({sortScripts,viewScripts,sortButtonTable,valueId,checked,turnSortButtonInit,turnSortButton}) {
    const [buttonView, setButtonView] = useState(true)
    const [buttonSort, setButtonSort] = useState(true);
    const [userChoice, setUserChoice] = useState("")

    const idScript = useSelector(state => state.scriptId)

    const options = [
        {value: 'Дата создания', label: 'Дата создания'},
        {value: 'Название', label: 'Название'},
        {value: 'Последнее изменение', label: 'Последнее изменение'}
    ]
    
    // useEffect(()=>{
    //     sortButtonTable && setButtonSort(true)
    // },[sortButtonTable])

    useEffect(() => {
        turnSortButton && setButtonSort(true)
    },[turnSortButton])

    const handleClickSearch = (e) => {
        e.preventDefault()

    }
   
    const handleClickSort = () => {
        // if(userChoice === '') {console.log(userChoice);
        //     sortScripts(userChoice,buttonSort)
        //     setButtonSort(!buttonSort)
        // } else if(userChoice === 'Дата создания') {
        //     setButtonSort(!buttonSort)
        //     console.log('Дата создания')
        //     sortScripts(userChoice,buttonSort)
        // } else if(userChoice === 'Название') {
        //     setButtonSort(!buttonSort)
        //     console.log('Название')
        //     sortScripts(userChoice,buttonSort)
        // } else if(userChoice === 'Последнее изменение') {
        //     setButtonSort(!buttonSort)
        //     console.log('Последнее изменение')
        //     sortScripts(userChoice,buttonSort)
        // } 
        if(userChoice === "" || userChoice) {
            setButtonSort(!buttonSort)
            sortScripts(userChoice,buttonSort)
        }
        turnSortButtonInit(false)
    }
    const handleChange = (choice) => {
        setUserChoice(choice.value)
        setButtonSort(true);
    }
    const handleClickView = () => {
        setButtonView(!buttonView)
        viewScripts(buttonView)
        console.log('view');
    }
    
    return ( 
        <div className={css.Toolbar}>
            <div className={css.items}>
                <div className={css.item_left}>
                    <LinkButton route={''} variant={'btnExit'}>
                       <img src={iconExit} alt="exit" className={css.iconExit}/>
                        <span>Выход</span>
                    </LinkButton>
                    <form action="" className={css.form}>
                        <div className={`${css.form__inner}`}>
                        <label className={css.label}>
                            <input type="text" className={css.search} id="search" placeholder="Поиск по названию"/>
                        </label>
                        <Button handleClick={handleClickSearch} type="submit" variant="searchOperator">
                            <img src={search} alt="search" />
                        </Button>
                        </div>   
                    </form>
                </div>
                <div className={css.item_right}>
                        <Button handleClick={handleClickSort} type="button" variant={buttonSort ? 'sortOperator' : 'sortOperatorActive'}>
                          <img src={iconSort} alt="exit" className={css.iconSort}/>
                        </Button>
                    <ReactSelect
                        options={options}
                        placeholder="Сортировать"
                        styles={styles}
                        components={{
                            // IndicatorSeparator: () => null,
                        }}
                        onChange={handleChange}
                    />
                    <Button handleClick={handleClickView} type="button" variant={buttonView ? 'viewOperator' : 'viewOperatorActive'}>
                          <img src={buttonView ? iconViewCard : iconViewTable} alt="icon-button-view" className={css.iconView}/>
                    </Button>
                    <LinkButton
                    route={'dialog'}
                    variant={
                        // valueId !== '' ? 'newDialogActive' : 'newDialog'
                        idScript !== null ? 'newDialogActive' : 'newDialog'
                    }
                    >
                        <img src={iconCross} alt="icon-button-view" className={css.iconCross}/>
                        Новый диалог
                    </LinkButton>
                </div>
            </div>
        </div>
     );
}

export default ToolbarOperatorMain;