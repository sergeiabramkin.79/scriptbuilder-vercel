import { shallowEqual, useSelector } from 'react-redux'
import Login from '../Login';

const ProtectedRoute = ({ children }) => {
    const isAuth = useSelector(state => state.user.authLevel, shallowEqual)

    if(!isAuth) {
        return <Login />
    }

    return children
}

export default ProtectedRoute;
