import css from './Notifications.module.css';
import notificationIcon from '../../Assets/images/notification-icon.png'


const Notifications = ({ listClass }) => {
    const notifications = [
        {
            id: `1${listClass}`,
            img: notificationIcon,
            title: '%author’s name%',
            text: 'Lorem ipsum dolor sit amet consectetur. Dictumst nunc urna nisl ut vulputate et mauris urna et. Proin quis diam lobortis sodales.',
            date: '1 Jan 2023',
            time: '15:32'
        },
        {
            id: `2${listClass}`,
            img: notificationIcon,
            title: '%author’s name%',
            text: 'Lorem ipsum dolor sit amet consectetur. Dictumst nunc urna nisl ut vulputate et mauris urna et. Proin quis diam lobortis sodales.',
            date: '1 Jan 2023',
            time: '15:32'
        }
    ]

    const cls = {
        adminList: `${css.adminList}`,
        headerList: `${css.headerList}`
    }

    const handleClick = () => console.log('list click')

    return (
        <ul className={cls[`${listClass}`]}>
            {   
                notifications.map(item => {
                    return (
                        <li className={css.notification} key={item.id} onClick={handleClick}>
                            <div className={css.notificationTop}>
                                <img src={item.img} alt="" width={76} height={76} />
                                <div className={css.notificationText}>
                                    <h3>
                                        {item.title}
                                    </h3>
                                    <p>
                                        {item.text}
                                    </p>
                                </div>
                            </div>
                            <div className={css.notificationBottom}>
                                <p>
                                    {item.date}
                                </p>
                                <p>
                                    {item.time}
                                </p>
                            </div>
                        </li>
                    )
                })
            }
        </ul>
        
    )
    
}

export default Notifications;