import style from "./style.module.css";
import { Link } from 'react-router-dom';

const Plus = () => {
  return (
    <div className={style.container_plus}>
      <Link to="/constructor">
        <div className={style.plus}>+</div>
      </Link>
    </div>
  );
};

export default Plus;
