import React from 'react';
import { useSelector } from 'react-redux';
import Replica from "../../Components/Replica";

const BlockOperator = () => {
  const phrases = useSelector((state) => state.blocks.blocks[2]?.phrase || 'пусто');

  return (
    <>
      {phrases}
    </>
  );
};

export default BlockOperator;
