import React from 'react';
import { useSelector } from 'react-redux';

const Replica = () => {
  // Прямо обращаемся к фразе второго блока
  const phrase = useSelector((state) => state.blocks.blocks[1]?.phrases[0]?.phrase || '');

  return (
    <>
      {phrase}
    </>
  );
};

export default Replica;
