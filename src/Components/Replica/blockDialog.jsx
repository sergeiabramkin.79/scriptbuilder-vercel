// Ваш компонент на странице
import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import fetchData from "../../hooks/asyncAction";

const BlockDialog = () => {
  const data = useSelector((state) => state.init.data?.phrases?.[0]?.phrase);
/*   const loading = useSelector((state) => state.initSlice.loading);
  const error = useSelector((state) => state.initSlice.error); */
  const dispatch = useDispatch();

  useEffect(() => {
    // Загрузите данные из API при монтировании компонента
    dispatch(fetchData());
  }, [dispatch]);

/*   if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }
 */
  if (!data) {
    return <div>no data!</div>;
  }

  return (
    <div>
      <h1>Data from Store:</h1>
      <div>{data? data: "no data!"}</div>
    </div>
  );
};

export default BlockDialog;
