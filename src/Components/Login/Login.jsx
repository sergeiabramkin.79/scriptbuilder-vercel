import css from "./login.module.css"
// import { useState } from "react";
import close from "../../Assets/images/close.svg";
import { useNavigate } from "react-router-dom";
// import useAxios from '../../hooks/useAxios'
// import axios from "axios";
// import Cookies from "js-cookie";

import LoginForm from '../Forms/LoginForm'

const Login = () => {
    const navigate = useNavigate()

    function handleronClick() {
        // reset({
        //     username: "",
        //     password: ""
        // })
        navigate('/')
    }


    return (
        <div className={css.wrapper_open} >
            <div className={css.registration} >
                <img src={close} alt="close" className={css.cross} onClick={handleronClick} />
                <div className={css.itemleft}>
                    <div className={css.title}>
                        <h2>Пожалуйста, введите свои данные для входа в систему</h2>
                    </div>
                </div>
                <div className={css.itemright}>
                    <LoginForm />
                </div>
            </div>
        </div>
    );
}

export default Login;