import { useState } from "react";
import css from "./Header.module.css";
import { NavLink, Link, useLocation } from "react-router-dom";
import { shallowEqual, useSelector } from "react-redux";
import Notifications from "../Notifications";
import { Button } from "../Button/Button";
import Avatar from "../../Assets/images/header-avatar.svg";
import Bell from "../../Assets/images/header-bell.svg";
import close from "../../Assets/images/close.svg";
// import logo from "../../Assets/images/logo.png"

const Header = () => {
  const [isOpen, setIsOpen] = useState(false);
  const { pathname } = useLocation();
  const { userName } = useSelector((state) => state.user, shallowEqual);

    // for conditional rendering of Help link
    const hasHelp = ['/operator', '/coordinator', '/administrator', '/constructor','/administrator/templates','/administrator/allscripts','/administrator/drafts','/administrator/comments']

  const handleClick = () => setIsOpen(!isOpen);

    const getTitle = () => {
        switch (pathname) {
            case '/operator':
                return 'Оператор'
            case '/dialog':
                return 'Диалог'
            case '/coordinator':
                return 'Координатор'
            case '/help':
                return 'Центр помощи'
            case '/administrator':
                return 'Администратор'
            case '/mainadministrator':
                return 'Главный администратор'
            case '/constructor':
                return 'Конструктор скриптов'
            case '/administrator/templates':
                return 'Шаблоны'
            case '/administrator/allscripts':
                return 'Все скрипты'
            case '/administrator/drafts':
                        return 'Черновики'
            case '/administrator/comments':
                return 'Комментарии от оператора'
            default:
                return null
        }
    }
  // };

    const getHeaderRight = () => {
        if (!userName || pathname === '/' || pathname === '/operator') {
            return (
                <div className={css.help}>
                    <Link to='/help'>Центр помощи</Link>
                </div>
            )
        } else {
            return (
                <div
                    className={
                        hasHelp.includes(pathname) ?
                            css.headerRight :
                            `${css.headerRight} ${css.headerRightNoHelp}`}>
                    <div className={css.avatar}>
                        <img className={css.icon} src={Avatar} alt="аватар" />
                    </div>
                    <div className={css.welcome}>
                        Привет, {userName}
                    </div>
                    <div
                        className={hasHelp.includes(pathname) ? css.notify : css.notifyNoHelp}
                        onClick={handleClick}>
                        <img className={css.icon} src={Bell} alt="колокольчик" width='24' height='24' />
                        <div className={css.counter}>
                            <span>2</span>
                        </div>
                        {isOpen ?
                            <div className={css.dropdown}>
                                <div className={css.dropdownTop}>
                                    <h2 className={css.headerNotifyTitle}>Уведомления</h2>
                                    <Button type={'button'} variant={'close'} handleClick={handleClick}>
                                        <img src={close} alt="закрыть окно" className={css.cross} width={24} height={24} />
                                    </Button>
                                </div>
                                <Notifications
                                    listClass={'headerList'}
                                />
                            </div>
                            : null}
                    </div>
                    {
                        hasHelp.includes(pathname) &&
                        <div className={css.help}>
                            <Link to='/help'>Центр помощи</Link>
                        </div>
                    }
                </div>
            )
        }
    }
  return (
    <header className={css.header}>
      <div className={css.container}>
        <div className={css.headerLeft}>
          <NavLink to="/" className={css.logo}>
            <div className={css.logo_img}></div>
            <div className={css.logo_name}>script <br/> builder</div>
          </NavLink>
          <h1 className={css.headerTitle}>{getTitle()}</h1>
        </div>
        {getHeaderRight()}
      </div>
    </header>
  );
};

export default Header;
