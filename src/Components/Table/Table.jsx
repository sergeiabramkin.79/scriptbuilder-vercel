import css from './Table.module.css';


const Table = ({ tableData, variant }) => {
    const variants = {
        default: '',
        shortFirstCol: `${css.shortFirstCol}`
    }


    return (
        <table>
            <tbody>
                {
                    tableData.map(row => {
                        return (
                            <tr key={row.id}>
                                {
                                    Object.values(row).map((item, index) => {
                                        return (
                                            index !== 0 &&
                                            <td
                                                key={index}
                                                className={variants[`${variant}`]}
                                            >
                                                {item}
                                            </td>
                                        )
                                    })
                                }
                            </tr>
                        )
                    })
                }
            </tbody>
        </table>
    )
}

export default Table;