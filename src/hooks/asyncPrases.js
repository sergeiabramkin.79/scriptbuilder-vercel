// Импортируем библиотеки
import { fetchDataRequest } from "../store/slices/dataSlice";
import axios from "axios";


const fetchData = (uuid) => {

  return async function (dispatch) {

    const apiUrl = `https://api.doscript.pnpl.tech/api/v2/phrases/${uuid}`;

    // Используем axios для запроса
    const response = await axios.get(apiUrl);
    const data = response.data;
    dispatch(fetchDataRequest(data)); // Диспетчеризация action с успешным результатом
  };
};
export default fetchData;

