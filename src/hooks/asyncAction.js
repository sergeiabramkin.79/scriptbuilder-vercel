// Импортируем библиотеки
import { fetchDataRequest, fetchDataSuccess, fetchDataFailure } from '../store/slices/initSlice';
import axios from 'axios';


const fetchData = () => {
  return async function(dispatch) {
    dispatch(fetchDataRequest()); // Диспетчеризация action о начале загрузки

    try {
      const apiUrl = 'https://api.doscript.pnpl.tech/api/v2/script/5dd899bd-3354-4c5c-9be8-ed812836d60e';

      // Используем axios для запроса
      const response = await axios.get(apiUrl);
      const data = response.data;

      dispatch(fetchDataSuccess(data)); // Диспетчеризация action с успешным результатом
    } catch (error) {
      console.error('Error fetching data:', error);
      dispatch(fetchDataFailure(error.message)); // Диспетчеризация action с ошибкой
    }
  }
}

export default fetchData;